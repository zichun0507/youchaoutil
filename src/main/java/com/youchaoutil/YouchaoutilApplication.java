package com.youchaoutil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.youchaoutil.*.mapper")
public class YouchaoutilApplication {

    public static void main(String[] args) {
        SpringApplication.run(YouchaoutilApplication.class, args);
    }

}
