package com.youchaoutil.aspects;

import com.youchaoutil.utils.IPUtil;
import com.youchaoutil.utils.LogUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * 系统日志输入
 * 
 * @ClassName: LogAspect
 * @author: 作者邮箱：zichun0507@务有限公司。
 */
@Aspect
@Component
public class LogAspect {

	/**
	 * 记录Controller层所有操作
	 * 
	 * @author zichun
	 * @Date 2019年3月9日 上午9:40:57
	 * @version v1.0.0.0
	 */
	//自定义切面注解
	@Pointcut("execution(* com.youchaoutil.controller..*.*(..))")
	public void logAspect() {

	}

	/**
	 * 环绕通知，执行方法，记录日志
	 * 
	 * @author zichun
	 * @Date 2019年3月9日 上午9:42:38
	 * @version v1.0.0.0
	 * @throws Throwable
	 */
	@Around("logAspect()")
	public Object doLogAround(ProceedingJoinPoint point) throws Throwable {
		LogUtils.hr();
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		LogUtils.msg("请求IP地址为：" + IPUtil.getClientIpAddress(request));
		String requestUrl = request.getRequestURL().toString();
		LogUtils.msg("请求路径：" + requestUrl);
		if(!requestUrl.contains("/notify")) {
			LogUtils.msg("参数为：" + Arrays.toString(point.getArgs()));
		}
		Long beginTime = System.currentTimeMillis();
		Object proceed = point.proceed();
		Long endTime = System.currentTimeMillis();
		LogUtils.msg("方法执行时间为：" + (endTime - beginTime) + "毫秒");
		return proceed;
	}

}
