package com.youchaoutil.controller;

import com.youchaoutil.controller.form.order.QueryOrderInfoForm;
import com.youchaoutil.controller.form.order.UpdateIsDeletedForm;
import com.youchaoutil.controller.result.ResponseMessage;
import com.youchaoutil.controller.result.ResponseResult;
import com.youchaoutil.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description: TODO
 * @Author: zichun
 * @create: 2019-09-24 11:56
 * @Copyright: 2019 www.591ba.com Inc. All rights reserved.
 * 注意：这不是一个开源项目，禁止非法传播
 **/
@RestController
@CrossOrigin
@RequestMapping("/youchaoutil/api/v1.0/order/")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("query")
    public ResponseResult<Object> queryOrder(@Validated @RequestBody QueryOrderInfoForm form) {
        return new ResponseResult<>(orderService.queryOrderInfo(form));
    }

    @PostMapping("updateIsDeleted")
    public ResponseResult<Object> updateIsDeleted(@Validated @RequestBody UpdateIsDeletedForm form) {
        if (orderService.updateIsDeleted(form) > 0) {
            return new ResponseResult<>(ResponseMessage.OK);
        }
        return new ResponseResult<>(ResponseMessage.INTERNAL_SERVER_ERROR);
    }

    /**
     * 功能描述: <br>
     * Excel导出订单
     *
     * @Author: zichu
     * @Date: 2019/10/14 17:34
     */
    @GetMapping("excelOrder")
    public void excelOrder(
            String orderSn,
            String bathOrderSn,
            String transactionId,
            Integer userId,
            Integer status,
            String beginTime,
            String endTime,
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        orderService.excelOrder(orderSn, bathOrderSn, transactionId, userId, status, beginTime, endTime, request, response);
    }


    /**
     * 功能描述: <br>
     * 退款
     *
     * @Author: zichu
     * @Date: 2019/11/11 19:55
     */
    @GetMapping("alipayRefundBy")
    public ResponseResult<Object> alipayRefundBy(@RequestParam(name = "sn")  String sn) {
        if (orderService.alipayRefundByOrderSn(sn) != null) {
            return new ResponseResult<>();
        }
        return new ResponseResult<>(503, "未知错误，请记录表格！");
    }

}
