package com.youchaoutil.controller.form.base;

import cn.hutool.core.bean.BeanUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @Description: 表单的基类
 * @Author: zichun
 * @create: 2019-09-24 11:56
 * @Copyright: 2019 www.591ba.com Inc. All rights reserved.
 * 注意：这不是一个开源项目，禁止非法传播
 **/
@Data
public class BaseForm implements Serializable {

    /**
     * 时间戳
     */
    private String timestamp;


    public Map<String, Object> toMap() {
        return BeanUtil.beanToMap(this, false, true);
    }
}
