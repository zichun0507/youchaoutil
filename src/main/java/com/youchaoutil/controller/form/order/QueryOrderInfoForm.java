package com.youchaoutil.controller.form.order;

import com.youchaoutil.controller.form.base.BaseForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: TODO
 * @Author: zichun
 * @create: 2019-09-24 11:53
 * @Copyright: 2019 www.591ba.com Inc. All rights reserved.
 * 注意：这不是一个开源项目，禁止非法传播
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QueryOrderInfoForm extends BaseForm {

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 商品合并编号
     */
    private String bathOrderSn;

    /**
     * 支付编号
     */
    private String transactionId;

    /**
     * 优潮用户id
     */
    private Integer userId;

    /**
     * 订单状态
     */
    private Integer status;

    /**
     * 时间的查询类型，1下单时间 2支付时间
     */
    private Integer timeType;

    private String beginTime;

    private String endTime;

    private Integer pageSize = 10;

    private Integer pageNum = 1;

}
