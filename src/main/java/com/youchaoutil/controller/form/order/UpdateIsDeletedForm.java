package com.youchaoutil.controller.form.order;

import com.youchaoutil.controller.form.base.BaseForm;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: zichun
 * @create: 2019-10-14 13:45
 * @Copyright: 2019 www.591ba.com Inc. All rights reserved.
 * 注意：这不是一个开源项目，禁止非法传播
 **/
@Data
public class UpdateIsDeletedForm extends BaseForm {

    /**
     * 订单ID
     */
    private Long orderId;

}
