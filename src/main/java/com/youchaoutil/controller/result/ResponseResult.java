package com.youchaoutil.controller.result;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
public class ResponseResult<T> implements Serializable {
    private static final long serialVersionUID = 2719931935414658118L;

    private String status;

    private String message;

    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date timestamp;

    private String token;

    private T data;



    public ResponseResult(Exception e) {
        this("400", e.getMessage());
    }

    public ResponseResult() {
        this(200, "请求成功！");
    }



    public ResponseResult(String status, String message) {
        this.status = status;
        this.message = message;
        this.timestamp = new Date();
    }


    public ResponseResult(Integer status, String message) {
        this.status = String.valueOf(status);
        this.message = message;
        this.timestamp = new Date();
    }


    public ResponseResult(String status, String message, T data) {
        this(status, message);
        this.data = data;
    }

    public ResponseResult(T data) {
        this(200, "请求成功！");
        this.data = data;
    }


    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("status", status);
        if (message != null) {
            map.put("message", message);
        }
        if (timestamp != null) {
            map.put("timestamp", timestamp);
        }
        if (data != null) {
            map.put("data", data);
        }
        return map;
    }

}
