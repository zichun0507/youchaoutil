package com.youchaoutil.error;

import com.alipay.api.AlipayApiException;
import com.youchaoutil.controller.result.ResponseResult;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 全局异常处理类
 * 
 * @author ZiChun
 * @data 2018年12月10日
 * @version v1.0.0.0
 */
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	/**
	 * 定义要捕获的异常 可以多个 @ExceptionHandler({})
	 *
	 * @param request
	 *            request
	 * @param e
	 *            exception
	 * @param response
	 *            response
	 * @return 响应结果
	 */
	@ExceptionHandler(BoyaException.class)
	public ResponseResult<?> boyaExceptionHandler(HttpServletRequest request, final Exception e,
												  HttpServletResponse response) {
		response.setStatus(HttpStatus.OK.value());
		BoyaException exception = (BoyaException) e;
		logger.error("boya异常捕获器执行，捕获异常信息：" + e.getMessage());
		return new ResponseResult<>(exception.getCode(), exception.getMessage());
	}



	/**
	 * 捕获 RuntimeException 异常 TODO 如果你觉得在一个 exceptionHandler 通过 if (e instanceof
	 * xxxException) 太麻烦 TODO 那么你还可以自己写多个不同的 exceptionHandler 处理不同异常
	 *
	 * @param request
	 *            request
	 * @param e
	 *            exception
	 * @param response
	 *            response
	 * @return 响应结果
	 */
	@ExceptionHandler(RuntimeException.class)
	public ResponseResult<?> runtimeExceptionHandler(HttpServletRequest request, final Exception e,
			HttpServletResponse response) {
		response.setStatus(HttpStatus.OK.value());
		RuntimeException exception = (RuntimeException) e;
		logger.error("运行异常捕获器执行，捕获异常信息如下：", e);
		return new ResponseResult<>(400, exception.getMessage());
	}

	/**
	 * 通用的接口映射异常处理方
	 */
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		logger.error("通用异常捕获器执行，捕获异常信息如下：", ex);
		return new ResponseEntity<Object>(new ResponseResult<>(status.value(), ex.getMessage()), HttpStatus.OK);
	}

}
