package com.youchaoutil.order.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * YC_ORDER
 * 
 * @author zichun
 * @version 1.0.0 2019-09-24
 */
@Entity
@Table(name = "yc_order")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderEntity implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1034584868126163893L;


    /** 订单id */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_ID", unique = true, nullable = false, length = 10)
    private Long orderId;

    /** 订单编号 */
    @Column(name = "ORDER_SN", unique = true, nullable = false, length = 20)
    private String orderSn;

    /** 批量结算的编号 */
    @Column(name = "BATH_ORDER_SN", nullable = true, length = 40)
    private String bathOrderSn;

    /** 用户id */
    @Column(name = "USER_ID", nullable = false, length = 10)
    private Long userId;

    /** storeId */
    @Column(name = "STORE_ID", nullable = false, length = 10)
    private Integer storeId;

    /** 订单状态 */
    @Column(name = "STATUS", nullable = false, length = 3)
    private Integer status;

    /** 支付code */
    @Column(name = "PAY_CODE", nullable = true, length = 32)
    private String payCode;

    /** 支付方式名称 */
    @Column(name = "PAY_NAME", nullable = true, length = 120)
    private String payName;

    /** 发票抬头 */
    @Column(name = "INVOICE", nullable = true, length = 256)
    private String invoice;

    /** 商品总价 */
    @Column(name = "GOODS_AMOUNT", nullable = false, length = 10)
    private Double goodsAmount;

    /** 商品总数量 */
    @Column(name = "GOODS_QUANTITY", nullable = false, length = 10)
    private Integer goodsQuantity;

    /** 邮费 */
    @Column(name = "SHIPPING_AMOUNT", nullable = false, length = 10)
    private Double shippingAmount;

    /** 实际支付的钱 */
    @Column(name = "PAY_AMOUNT", nullable = false, length = 10)
    private Double payAmount;

    /** 用多少积分抵扣订单 */
    @Column(name = "POINTS_AMOUNT", nullable = false, length = 10)
    private Double pointsAmount;

    /** 优惠券ID */
    @Column(name = "COUPON_ID", nullable = false, length = 10)
    private Integer couponId;

    /** 订单总价 */
    @Column(name = "DISCOUNT_AMOUNT", nullable = false, length = 10)
    private Double discountAmount;

    /** 应付款金额 */
    @Column(name = "ORDER_AMOUNT", nullable = false, length = 10)
    private Double orderAmount;

    /** 下单时间 */
    @Column(name = "ADD_TIME", nullable = false, length = 10)
    private Long addTime;

    /** 支付时间 */
    @Column(name = "PAY_TIME", nullable = false, length = 10)
    private Long payTime;

    /** 发货时间 */
    @Column(name = "LOGISTIC_TIME", nullable = false, length = 10)
    private Integer logisticTime;

    /** 用户确认时间 */
    @Column(name = "CONFIRM_TIME", nullable = false, length = 10)
    private Integer confirmTime;

    /** 评价时间 */
    @Column(name = "EVALUATE_TIME", nullable = false, length = 10)
    private Integer evaluateTime;

    /** 第三方平台交易流水号 */
    @Column(name = "TRANSACTION_ID", nullable = false, length = 255)
    private String transactionId;

    /** 用户备注 */
    @Column(name = "REMARK", nullable = true, length = 255)
    private String remark;

    /** 取消订单时的原因 */
    @Column(name = "CANCEL_REASON", nullable = true, length = 255)
    private String cancelReason;

    /** 取消订单的编号 */
    @Column(name = "CANCEL_SN", nullable = true, length = 20)
    private String cancelSn;

    /** 用户假删除标识,1:删除,0未删除 */
    @Column(name = "IS_DELETED", nullable = true)
    private Integer isDeleted;

    /** 提醒发货,0-未提醒,1-提醒 */
    @Column(name = "IS_REMIND", nullable = true)
    private Integer isRemind;

    /** 是否是返利 */
    @Column(name = "IS_REBATE", nullable = false, length = 3)
    private Integer isRebate;

    /** 0-普通订单,1-秒杀订单 */
    @Column(name = "ORDER_TYPE", nullable = true, length = 3)
    private Integer orderType;

    /** 对应活动ID */
    @Column(name = "ACTIVITY_ID", nullable = true, length = 10)
    private Integer activityId;

}