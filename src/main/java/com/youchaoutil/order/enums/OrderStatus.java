package com.youchaoutil.order.enums;

public enum OrderStatus {
    QUXIAO(99,"已取消"),
    DAIZHIFU(100,"等待付款"),
    DAIFAHUO(101,"已付款"),
    YIFAHUO(103,"已发货"),
    YIWANCHENG(104,"已完成"),
    ;

    private Integer status;
    private String desc;


    OrderStatus(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public Integer status() {
        return status;
    }

    public String desc() {
        return desc;
    }
}
