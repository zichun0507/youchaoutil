package com.youchaoutil.order.mapper;

import com.youchaoutil.order.entity.OrderEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Description: TODO
 * @Author: zichun
 * @create: 2019-09-24 11:43
 * @Copyright: 2019 www.591ba.com Inc. All rights reserved.
 * 注意：这不是一个开源项目，禁止非法传播
 **/
public interface OrderMapper extends Mapper<OrderEntity> {

    @Select("select user_id as userId, mobile, nickname from yc_user where user_id = #{userId}")
    List<Map<String, Object>> getUserInfoByUserId(@Param("userId") Long userId);


    @SelectProvider(type = OrderMapperSQL.class, method = "getUserInfoByUserIdsSQL")
    List<Map<String, Object>> getUserInfoByUserIds(@Param("userIds") List<Long> userIds);


    class OrderMapperSQL {
        public String getUserInfoByUserIdsSQL(@Param("userIds") List<Long> userIds){
            Object[] objects = userIds.toArray();

            String sql = "select user_id as userId, mobile, nickname from yc_user where user_id in ";

            return null;
        }

    }

}
