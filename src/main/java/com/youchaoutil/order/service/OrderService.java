package com.youchaoutil.order.service;

import com.youchaoutil.controller.form.order.QueryOrderInfoForm;
import com.youchaoutil.controller.form.order.UpdateIsDeletedForm;
import com.youchaoutil.order.vo.OrderVO;
import com.youchaoutil.page.PageInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface OrderService {

    /**
     * 功能描述: <br>
     *    查询优潮订单信息
     * @Author: zichu
     * @Date: 2019/9/24 12:26
     */
    PageInfo<List<OrderVO>> queryOrderInfo(QueryOrderInfoForm form);

    Integer updateIsDeleted(UpdateIsDeletedForm form);

    /**
     * 功能描述: <br>
     *  优潮订单导出
     * @Author: zichu
     * @Date: 2019/10/14 17:45
     */
    void excelOrder(String orderSn, String bathOrderSn, String transactionId, Integer userId, Integer status,
                    String beginTime, String endTime, HttpServletRequest request, HttpServletResponse response) throws IOException;


    Boolean alipayRefundByOrderSn(String sn);
}
