package com.youchaoutil.order.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.excel.EasyExcel;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.youchaoutil.controller.form.order.QueryOrderInfoForm;
import com.youchaoutil.controller.form.order.UpdateIsDeletedForm;
import com.youchaoutil.error.BoyaException;
import com.youchaoutil.order.entity.OrderEntity;
import com.youchaoutil.order.enums.OrderStatus;
import com.youchaoutil.order.mapper.OrderMapper;
import com.youchaoutil.order.service.OrderService;
import com.youchaoutil.order.vo.OrderExcelVO;
import com.youchaoutil.order.vo.OrderVO;
import com.youchaoutil.page.PageInfo;
import com.youchaoutil.utils.AliPayUtils;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @Description: TODO
 * @Author: zichun
 * @create: 2019-09-24 11:45
 * @Copyright: 2019 www.591ba.com Inc. All rights reserved.
 * 注意：这不是一个开源项目，禁止非法传播
 **/
@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Override
    public PageInfo<List<OrderVO>> queryOrderInfo(QueryOrderInfoForm form) {
        String bathOrderSn = form.getBathOrderSn();
        String orderSn = form.getOrderSn();
        String transactionId = form.getTransactionId();
        Integer pageNum = form.getPageNum();
        Integer pageSize = form.getPageSize();
        Integer userId = form.getUserId();
        String beginTime = form.getBeginTime();
        String endTime = form.getEndTime();
        Integer status = form.getStatus();
        Example example = new Example(OrderEntity.class);
        if (StrUtil.isNotBlank(bathOrderSn)) {
            example.and().andEqualTo("bathOrderSn", bathOrderSn.trim());
        }
        if (StrUtil.isNotBlank(orderSn)) {
            example.and().andEqualTo("orderSn", orderSn.trim());
        }
        if (StrUtil.isNotBlank(transactionId)) {
            example.and().andEqualTo("transactionId", transactionId.trim());
        }
        if (StrUtil.isNotBlank(beginTime)) {
            example.and().andGreaterThanOrEqualTo("addTime", DateUtil.parse(beginTime.trim()).getTime() / 1000);
        }
        if (StrUtil.isNotBlank(endTime)) {
            example.and().andLessThanOrEqualTo("addTime", DateUtil.parse(endTime.trim()).getTime() / 1000);
        }
        if (userId != null && userId > 0) {
            example.and().andEqualTo("userId", userId);
        }
        if (status != null && status > 0) {
            example.and().andEqualTo("status", status);
        }
        Page<Object> page = null;
        if (pageNum != 0 && pageSize != 0) {
            page = PageHelper.startPage(pageNum, pageSize);
        }
        example.orderBy("addTime").desc();
        List<OrderEntity> orderEntities = orderMapper.selectByExample(example);
        List<OrderVO> orderVos = orderEntities.stream().map(t -> orderEntityToVo(t)).collect(Collectors.toList());
        PageInfo<List<OrderVO>> objectPageInfo;
        if (page == null) {
            objectPageInfo = new PageInfo<>(orderVos);
        } else {
            objectPageInfo = new PageInfo<>(orderVos, page);

        }
        return objectPageInfo;
    }

    @Override
    public Integer updateIsDeleted(UpdateIsDeletedForm form) {
        Long orderId = form.getOrderId();
        OrderEntity entity = OrderEntity.builder()
                .orderId(orderId)
                .isDeleted(0)
                .build();
        return orderMapper.updateByPrimaryKeySelective(entity);
    }

    @Override
    public void excelOrder(String orderSn, String bathOrderSn, String transactionId, Integer userId, Integer status,
                           String beginTime, String endTime, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        QueryOrderInfoForm queryOrderInfoForm = QueryOrderInfoForm.builder()
                .orderSn(orderSn)
                .bathOrderSn(bathOrderSn)
                .transactionId(transactionId)
                .userId(userId)
                .status(status)
                .beginTime(beginTime)
                .endTime(endTime)
                .pageNum(0)
                .pageSize(0)
                .build();
        List<List<OrderVO>> data = queryOrderInfo(queryOrderInfoForm).getList();


        String fileName = UUID.randomUUID().toString().replace("-", "") + ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        EasyExcel.write(response.getOutputStream(), OrderExcelVO.class)
                .sheet("模板")
                .doWrite(data);
    }

    public OrderVO orderEntityToVo(OrderEntity entity) {
        OrderVO vo = new OrderVO();
        String[] noCopyProperties = new String[]{"status", "addTime", "payTime", "confirmTime", "evaluateTime", "logisticTime"};
        BeanUtil.copyProperties(entity, vo, noCopyProperties);
        Integer status = entity.getStatus();
        // 排断属性
        switch (status) {
            case 99:
                vo.setStatus(OrderStatus.QUXIAO.desc());
                break;
            case 100:
                vo.setStatus(OrderStatus.DAIZHIFU.desc());
                break;
            case 101:
                vo.setStatus(OrderStatus.DAIFAHUO.desc());
                break;
            case 103:
                vo.setStatus(OrderStatus.YIFAHUO.desc());
                break;
            case 104:
                vo.setStatus(OrderStatus.YIWANCHENG.desc());
                break;
            default:
                break;
        }
        Long addTime = entity.getAddTime();
        Long payTime = entity.getPayTime();
        Integer confirmTime = entity.getConfirmTime();
        Integer evaluateTime = entity.getEvaluateTime();
        Integer logisticTime = entity.getLogisticTime();
        vo.setAddTime(addTime <= 0 ? null : new DateTime(addTime * 1000L));
        vo.setPayTime(payTime <= 0 ? null : new DateTime(payTime * 1000L));
        vo.setConfirmTime(confirmTime <= 0 ? null : new DateTime(confirmTime * 1000L));
        vo.setEvaluateTime(evaluateTime <= 0 ? null : new DateTime(evaluateTime * 1000L));
        vo.setLogisticTime(logisticTime <= 0 ? null : new DateTime(logisticTime * 1000L));
        vo.setIsDeletedDesc(entity.getIsDeleted() == 1 ? "已删除" : "未删除");
        return vo;
    }


    /**
     * 功能描述: <br>
     * 根据订单号查询商品
     *
     * @Author: zichu
     * @Date: 2019/11/11 18:50
     */
    public OrderEntity getOrderByOrderSn(String orderSn) {
        Example example = new Example(OrderEntity.class);
        example.and().andEqualTo("orderSn", orderSn);
        OrderEntity entity = orderMapper.selectOneByExample(example);
        return entity;
    }

    /**
     * 功能描述: <br>
     * 根据订单号查询商品
     *
     * @Author: zichu
     * @Date: 2019/11/11 18:50
     */
    public OrderEntity getOrderByAliPaySn(String aliPaySn) {
        Example example = new Example(OrderEntity.class);
        example.and().andEqualTo("transactionId", aliPaySn);
        OrderEntity entity = orderMapper.selectOneByExample(example);
        return entity;
    }

    /**
     * 功能描述: <br>
     * 根据订单号退款
     *
     * @Author: zichu
     * @Date: 2019/11/11 18:46
     */
    @Override
    public Boolean alipayRefundByOrderSn(String sn) {
        String substring = sn.substring(0, 4);
        AlipayTradeRefundResponse alipayTradeRefundResponse;
        if ("1000".equals(substring)) {
            // 商家单号退款流程
            OrderEntity orderEntity = getOrderByOrderSn(sn);
            if (orderEntity == null) {
                throw new BoyaException(702, "未找到此订单，请检查是否输入错误！");
            }
            Integer status = orderEntity.getStatus();
            if (!(OrderStatus.QUXIAO.status().equals(status) || OrderStatus.DAIZHIFU.status().equals(status))) {
                throw new BoyaException(702, "订单状态正常，无法退款！！");
            }
            if (StrUtil.isNotBlank(orderEntity.getTransactionId())) {
                throw new BoyaException(702, "订单状态正常，无法退款！！");
            }
            String bathOrderSn = orderEntity.getBathOrderSn();
            String payAmount = orderEntity.getPayAmount().toString();
            alipayTradeRefundResponse = AliPayUtils.alipayTradeRefund(bathOrderSn, null, payAmount);
        } else {
            // 支付宝单号退款流程
            OrderEntity orderByAliPaySn = getOrderByAliPaySn(sn);
            if (orderByAliPaySn != null) {
                throw new BoyaException(702, "订单状态正常，无法退款！！");
            }

            AlipayTradeQueryResponse alipayTradeQueryResponse = AliPayUtils.alipayTradeQuery(null, sn);
            if(alipayTradeQueryResponse == null){
                throw new BoyaException(702, "退款失败，未找到该支付宝编号！");
            }
            String payAmount = alipayTradeQueryResponse.getTotalAmount();

            alipayTradeRefundResponse = AliPayUtils.alipayTradeRefund(null, sn, payAmount);
        }
        if (alipayTradeRefundResponse != null) {
            String code = alipayTradeRefundResponse.getCode();
            if (!"10000".equals(code)) {
                String subMsg = alipayTradeRefundResponse.getSubMsg();
                throw new BoyaException(500, subMsg);
            }
            return true;
        }
        return null;
    }

}
