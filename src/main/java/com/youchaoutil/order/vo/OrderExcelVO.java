package com.youchaoutil.order.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Description: TODO
 * @Author: zichun
 * @create: 2019-10-14 18:08
 * @Copyright: 2019 www.591ba.com Inc. All rights reserved.
 * 注意：这不是一个开源项目，禁止非法传播
 **/
@Data
public class OrderExcelVO {

    @ExcelProperty("订单iD")
    private Long orderId;

    @ExcelProperty("订单编号")
    private String orderSn;

    @ExcelProperty("合并订单号")
    private String bathOrderSn;

    @ExcelProperty("用户id")
    private Long userId;

    @ExcelProperty("订单状态")
    private String status;

    @ExcelProperty("支付code")
    private String payCode;

    @ExcelProperty("支付方式名称")
    private String payName;

    @ExcelProperty("商品总价")
    private Double goodsAmount;

    @ExcelProperty("商品总数量")
    private Integer goodsQuantity;

    @ExcelProperty("实付金额")
    private Double payAmount;

    @ExcelProperty("订单总价")
    private Double discountAmount;

    @ExcelProperty("应付款金额")
    private Double orderAmount;

    @ExcelProperty("下单时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    @ExcelProperty("支付时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;

    @ExcelProperty("发货时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date logisticTime;

    @ExcelProperty("用户确认时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date confirmTime;

    @ExcelProperty("评价时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date evaluateTime;

    @ExcelProperty("支付宝订单号")
    private String transactionId;

    @ExcelProperty("用户备注")
    private String remark;

    @ExcelProperty("取消原因")
    private String cancelReason;

    @ExcelProperty("取消订单编号")
    private String cancelSn;

    @ExcelProperty("是否删除")
    private Integer isDeleted;

    @ExcelProperty("是否删除")
    private String isDeletedDesc;
    
    
    
}
