package com.youchaoutil.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * YC_ORDER
 * 
 * @author zichun
 * @version 1.0.0 2019-09-24
 */
@Data
public class OrderVO implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 1034584868126163893L;

    /** 订单id */
    private Long orderId;

    /** 订单编号 */
    private String orderSn;

    /** 批量结算的编号 */
    private String bathOrderSn;

    /** 用户id */
    private Long userId;

    /** 订单状态 */
    private String status;

    /** 支付code */
    private String payCode;

    /** 支付方式名称 */
    private String payName;

    /** 商品总价 */
    private Double goodsAmount;

    /** 商品总数量 */
    private Integer goodsQuantity;

    /** 实际支付的钱 */
    private Double payAmount;

    /** 订单总价 */
    private Double discountAmount;

    /** 应付款金额 */
    private Double orderAmount;

    /** 下单时间 */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /** 支付时间 */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;

    /** 发货时间 */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date logisticTime;

    /** 用户确认时间 */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date confirmTime;

    /** 评价时间 */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date evaluateTime;

    /** 第三方平台交易流水号 */
    private String transactionId;

    /** 用户备注 */
    private String remark;

    /** 取消订单时的原因 */
    private String cancelReason;

    /** 取消订单的编号 */
    private String cancelSn;

    /** 用户假删除标识,1:删除,0未删除 */
    private Integer isDeleted;

    /** 用户假删除描述,删除,未删除 */
    private String isDeletedDesc;
}