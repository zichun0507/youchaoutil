package com.youchaoutil.utils;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 功能描述: <br>
 * 支付宝支付工具类
 *
 * @Author: zichu
 * @Date: 2019/8/4 9:11
 */
public class AliPayUtils {

    private static final Logger logger = LoggerFactory.getLogger(AliPayUtils.class);

    private static AlipayClient alipayClient = null;

    private static final String SERVER_URL = "https://openapi.alipay.com/gateway.do";

    private static final String APP_ID = "2018122762691989";

    private static final String APP_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCWKdtJz6BieZKIG+59u0Vgj4VRFPjBfraiWyBscn8gYge2ZqcH/hb98Pyhn7QjHN9nt7bZhqbTVvKEC7RNJlKm/VMBrs5trwZRPBvCbKnnxuZBy7Kp89HqpGCbk86RP9PU4O36BC9njydWtTOw5yxZedhYDH1Ukzjc2eBXGRDO1F31KRqvKy+KKqEOxfU1FHz4mM3P3U6+ZgTTYXYFzRH+L8pPZbJGkmyhY6Pcyx4X3rj4D3rzTkKPEiF7ftj9UCfsnHmQDOwfQtOnZcKadxdjwF1nHguPKtv0WHQiNhfw26/Zb+D6YTA7T0XgziiuiTHyJgI2rJOMvl1RbEDubkz5AgMBAAECggEAMlr872aR2RP9oGkFc+JN5JCcW1FWDcUJR66ZU+SdXuWgwnjwAE0jtqqOfUCJ7tBq1mQUDCQMY4ifsMof/1MfmmIO6NyO2tc7nhgzEuLPoyre4ieF1VovBalzwBgzyB+IULXQ9+tZXjfyMDB5khQjmimy8+vUR31LLzxc77g3G7Q1rsp8Fzwg4CMuZnn19exOwQr/5fwMf+ad92+b8yz7FoU2r0lWQDdrP7ParzyPEYA3Z0C4pfW3MRQbyY/zljWfOezNGe60scOlBTaHJbjuT2isRDLPWFcQDklYkdeo54Xrl1626BCkr7Wey6Hq0BgrEQwh5YTu6+pmFD6F9DMLVQKBgQDSXXP3CN+mMv8WBP30mVabrkQaZroQI/bfnNbaH08YvL8ZHzHY7SqdooTHMw0qncPIJNlsRPq9QEFSQhnT6Jo68I05AQmiqLc6dxL/MSTWUacvh1nAl9tQFFLoIff0Mhx/bXR1O0juTPFS9KtRw8+BOTpzMcxZKM8rLtxe+kUD1wKBgQC2vSHkNAgu7t5OmRDV6vXX6MD/fM0QIEw/dOOuTrxWPXbr9gSPs2H3nNXRdML30jkfiriIqBQRwbmbbU21mdzZnkzWdIa7w8VsnIgn8nJwUC9ug2qOyzRgm7u1XNuMoF6WIixjXLFwUrotGpsALQdxRS2MXgM0Ghcsd2E0Yc8brwKBgDm3dEK6B4q1ATHMatT6YT3TgVHxD2mwRGKzyGsOHgh+eLvuyCFpaMDZPy7cPeeMY5P9MD45asYT5BgqlKWEcxRqvYR/WWJsgeEjnG2x0RaQTpuGukRSJdUBzoFsGHjCavk3UbV8b5UaPTfyczK606rzxaqnZhasULLbZ5ETf23/AoGAPxPeBQWo5XpypjLTyuOF5Pdk10vfBPgBGc5hYSOwgxxIVn7faDMFWBF9BQYG3sBkE20w8YsDEnYKeYorTspDn6Ky5zvl3h5foXQO06susXIcTUtzlsKx278l9h6NcmdQ/wXgyJOQdDM6LKOIaXEhpom4hUFWXb+7zA+RMCGq+eUCgYEAsjA6Q8AYA4TlL10cduq3rdNkmABLv8D0EPhgPhQeZsLjeyu783mTXdKrYW7icnhaOYM7D0DU/5hegYfO/CwQcuxxH8JMW5KQpCNEh5U734MHBK/BoE3/Jq7DIhA2FOcfCj5HDjwxJ8VOBt1YFSaSxHL4F9OURLpYgKN1G9Mtcz8=";

    private static final String FORMAT = "json";

    private static final String CHARSET = "utf-8";

    private static final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAycUeKc05F9AuzIR5BQ15uCoG6GME06q/5vibu6KVbQdJjWiOyJjKrhaGlYH2HB05xr5AsREmu8ZrwpAWAQMfH+N+MPY4gEKA2nl6gn20e0dMlNBlOvqX8qb2NP4yhB4XMO8LSC2UD+RkrzBN4twEPEMK3us6t8fCzf70Zj/QipQTZ+UNV3sLwA/q9FY8zS0NHFmXtjlV1EJXXNAnjTkeBXwB1fWseEutEnCdaD2Iqc4QkmIAsaYWcdK9r5qbzcIEX7ozlqF99fyKjKCEfaSYQzinsC9k5NqG4pBMPisKh5K5PTuO2ylOlSz9ogkrVmFJUGy5Ib3h+Nkd+5/PAjeoRwIDAQAB";

    private static final String SIGN_TYPE = "RSA2";

    //普通订单回调地址
    private static final String NOTIFY_URL = "https://www.tlshike.com/back/putaway/pay_post_url\n";

    private static final String NOTIFY_TOKEN_URL = "http://api.youchaojingpin.com/api/v1.0/user/receiveAlipayAuthCode";

    static {
        getAlipayClient();
    }

    /**
     * 功能描述: <br>
     * 获得支付宝支付客户端
     *
     * @Return: com.alipay.api.AlipayClient
     * @Author: zichu
     * @Date: 2019/8/4 9:21
     */
    public static AlipayClient getAlipayClient() {
        if (alipayClient == null) {
            //实例化客户端
            alipayClient = new DefaultAlipayClient(SERVER_URL, APP_ID, APP_PRIVATE_KEY, FORMAT,
                    CHARSET, ALIPAY_PUBLIC_KEY, SIGN_TYPE);

        }
        return alipayClient;
    }

    /**
     * 功能描述: <br>
     * 统一收单线下交易查询
     *
     * @Param: [outTradeNo 优潮订单号 , tradeNo 支付宝订单号]
     * @Return: java.lang.String
     * @Author: zichu
     * @Date: 2019/8/4 10:31
     */
    public static AlipayTradeQueryResponse alipayTradeQuery(String outTradeNo, String tradeNo) {
        AlipayClient alipayClient = getAlipayClient();
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();
        if (StrUtil.isNotEmpty(tradeNo)) {
            model.setTradeNo(tradeNo);
        }
        model.setOutTradeNo(outTradeNo);
        request.setBizModel(model);
        AlipayTradeQueryResponse response;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            return null;
        }
        return response;
    }


    public static void main(String[] args) {
        AlipayTradeRefundResponse alipayTradeRefundResponse = alipayTradeRefund("3000165461912040913720", null, "369");
        System.out.println(JSON.toJSONString(alipayTradeRefundResponse));
    }

    /**
     * 功能描述: <br>
     * 支付宝退款
     *
     * @Author: zichu
     * @Date: 2019/11/11 18:33
     */
    public static AlipayTradeRefundResponse alipayTradeRefund(String orderSn, String aliPaySn, String refundAmount) {
        AlipayClient alipayClient = getAlipayClient();
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        AlipayTradeRefundModel alipayTradeRefundModel = new AlipayTradeRefundModel();
        if (StrUtil.isNotBlank(orderSn)) {
            alipayTradeRefundModel.setOutTradeNo(orderSn);
        }
        if (StrUtil.isNotBlank(aliPaySn)) {
            alipayTradeRefundModel.setTradeNo(aliPaySn);
        }
        alipayTradeRefundModel.setRefundAmount(refundAmount);
        request.setBizModel(alipayTradeRefundModel);
        AlipayTradeRefundResponse response;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            return null;
        }
        return response;
    }


    /**
     * 功能描述: <br>
     * 获得支付宝授权接口地址
     *
     * @param userId
     * @Author: zichu
     * @Date: 2019/9/18 12:04
     */
    public static String getAuthCodeUrl(String userId) {
        String uri = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?app_id="
                + APP_ID + "&scope=auth_user,auth_base&redirect_uri="
                + NOTIFY_TOKEN_URL + "&userId=" + userId;
        return uri;
    }

    /**
     * 功能描述: <br>
     * 获取支付宝access_token及userId。
     *
     * @Param: [code , grantType, refreshToken]
     * @Return: java.lang.String
     * @Author: zichu
     * @Date: 2019/9/18 12:07
     */
    public static AlipaySystemOauthTokenResponse alipaySystemOauthToken(AlipaySystemOauthTokenRequest request) {
        AlipayClient alipayClient = getAlipayClient();
        try {
            AlipaySystemOauthTokenResponse oauthTokenResponse = alipayClient.execute(request);
            return oauthTokenResponse;
        } catch (AlipayApiException e) {
            logger.error("获取支付宝的access_token及userId", e);
        }
        return null;
    }
}
