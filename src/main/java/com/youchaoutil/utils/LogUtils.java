package com.youchaoutil.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * 日志生成的工具类
 * 
 * @author ZiChun
 */
@Slf4j
public class LogUtils {
	/**
	 * 输出一道横线
	 * 
	 * @author zichun
	 * @Date 2019年3月7日 上午11:14:27
	 * @version v1.0.0.0
	 */
	public static void hr() {
		log.info("-------------------------------------------");
	}

	/**
	 * 输出指定内容
	 * 
	 * @param msg
	 * @author zichun
	 * @Date 2019年3月7日 上午11:15:00
	 * @version v1.0.0.0
	 */
	public static void msg(String msg) {
		log.info(msg);
	}

	/**
	 * 输出“---------------方法开始执行------------------------”
	 * 
	 * @author zichun
	 * @Date 2019年3月7日 上午11:16:50
	 * @version v1.0.0.0
	 */
	public static void beign() {
		log.info("---------------方法开始执行------------------------");
	}

	/**
	 * 输出“---------------方法end------------------------”
	 * 
	 * @author zichun
	 * @Date 2019年3月7日 上午11:16:50
	 * @version v1.0.0.0
	 */
	public static void end() {
		log.info("---------------方法end------------------------");
	}
}