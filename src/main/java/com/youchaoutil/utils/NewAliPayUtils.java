package com.youchaoutil.utils;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 功能描述: <br>
 * 支付宝支付工具类
 *
 * @Author: zichu
 * @Date: 2019/8/4 9:11
 */
public class NewAliPayUtils {

    private static final Logger logger = LoggerFactory.getLogger(NewAliPayUtils.class);

    private static AlipayClient alipayClient = null;

    private static final String SERVER_URL = "https://openapi.alipay.com/gateway.do";

    private static final String APP_ID = "2019032863743529";

    private static final String APP_PRIVATE_KEY = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCmNrM6SZi6yu/i4O22tB5cMpiisaz/iylQ5f5IwGCjTIL16LvQyyzt9wmwsZ7Ou2MDcZCGpyGNV0hsr1w7E+v+6jiKql8y7rNnl5YOtiVuNO+61z66TzS6QzqlPa3qubL+zjAALmfLLf5MuRZQRasOUC7z91pkrDRsd/pjK/iaACLBQ/ZTaXNlyfsABferyC7S7QnZJS3ndbhvr22ANOrs/YS5FbpECno70Xr7q+XR5LP74xvFBikk0iOPKozrepsLDu6Vcx/EjlPZfmVH499PSkfto0oiqk6FvfxWsWoYJIq3kUwOJSDFTDSU8FOTsOi8HIUXMlt9knkwwbfWy0ADAgMBAAECggEAJtlmLtuA3TgNtcPJDAkxh280CeLpgpkuQQBg3PRKIiECAyQwse6rQ7uYAjqh2cQmsmla1H4/819RSEX8krpnUJ/CM3b65VaxjCb8JdghC49H8S3aNYXJ1zdbf7/Y5KIe2jurMH+oNu87Hwer1XtTLfdD4jYQZLNzVV79oSxn4OaBh5LrgLicw9lH2nTwZ3pkvT3LKo2E2U+A85B6hnluCGAlOelUnDF7Ryctdf/EgfEHsF+ACKUPpPIiyfA4VmlcONmzCwYx4LKOISzEjOPlXIu+AgimqBGnSAfnfkQpg9ydmXHhUoRm09Bkc9MUyXpSXmQA76hJYD5L+zgv3Nu8sQKBgQDygTmLGSZHVO0LDetc5R8vhnLIVgbpQ6GeP0wU6/sjs4/eJevYIiqmQHl4XUIl4QHsWrHcAn+ojV01R+vegv/7hdf4T4AzOampHs2euIlacs783hxfKUXMIudMVnhwIW3tyUKlTFSRcYauObfSlH4DDMd6WjT76jrFxsGhtxOMfQKBgQCvdp2jBPtALgrEf3ovV4sC+0etb1HAkKMHyZ3WeVpb3grJeCWpwLUFEndRpgv8JM1cWKQOY51CfQV/RgZSlHAEU2nE9azq48u33qP6gfnq1S00Kx/1SleVtfrCv9GhfPHiUdJD4U4uUzdl/2CadQkviqi3HX/626VPLm9FvJYmfwKBgBKq+fgIPpcdJYZFUwn6pVTuP6XGYY/RYf6hlsTBibqs1vVd6/LeZnQX4IHwg+gQkypTrP9nvdYG09cHK1Qct4HwQrjdvduzo6jeMyv1vrUUw3RZ0MtOjQ3y3eVVfr37BNBaPuQkpRnxKFH/JXRzc+PeNvjnOs7tsAqLi1pckoetAoGAYo6yBPurVWWHTtB8BVObFdWncVfyB8Q2jw1Ex0/q3Wdfj8MZBLzl8S9qLkaivMK/R3vSyvJRRyOKju76ZjBSq1tfMe79bgL+Tw2T9gZEJxAPEBqZHOLhR4qsi+/LBHZ2N7XV8aMAkyzTFUvZK2lJ+5ZbJmA/ntz5gfyHLBi+DmkCgYBT1E3PbE3BeTvItniuT8DhOBw+jMiCob3aFXR/LumCOh7zu6Gb+6hOR+3Pioy+RNFMJFNuTEFbndZ9LUr84qi3v9jko0dhQ3YbA/fhP1SHXhykD3wuiDH89ZGTQtZyfyO3n1u8YCISEbLNkk4aURgGd73j4wH+55Lalj6k9I0gqA==";

    private static final String FORMAT = "json";

    private static final String CHARSET = "utf-8";

    private static final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvcC+uGXz+jcZcoy7HUw/qpdg9U9YtKdfLS6eiXVa6KZps96oZGJhjL0dxbp1GhZj60N1fD6ns8okw2y5g13eACtcXglnFKPTEYvCZzxKNeyAyItfdJu8G2CrCbyAuZpyigdYvWcs+NbqR4m9T+3WjHh3ST0yP4qAguLdZxtWZ6NtLoQD+NyrBMZyBsflHbi1PcZqCDyfIlgUT9fq8RsEcAH7OINBeS1dM2MUa/QLOfzlOU0D5KXQZJLBLHTRy3j3JjI9OizqUV4hpZdDB7qVnTJVMBuEdi+bZApWsWdnU5lnmFGgDb5HxN1C983s8b9wIZZ/P7//IOZvplJjV90PoQIDAQAB";

    private static final String SIGN_TYPE = "RSA2";

    //普通订单回调地址
    private static final String NOTIFY_URL = "http://api.youchaojingpin.com/api/v1.0/alipay/notify/notice";

    private static final String NOTIFY_TOKEN_URL = "http://api.youchaojingpin.com/api/v1.0/user/receiveAlipayAuthCode";
    static {
        getAlipayClient();
    }

    /**
     * 功能描述: <br>
     * 获得支付宝支付客户端
     *
     * @Return: com.alipay.api.AlipayClient
     * @Author: zichu
     * @Date: 2019/8/4 9:21
     */
    public static AlipayClient getAlipayClient() {
        if (alipayClient == null) {
            //实例化客户端
            alipayClient = new DefaultAlipayClient(SERVER_URL, APP_ID, APP_PRIVATE_KEY, FORMAT,
                    CHARSET, ALIPAY_PUBLIC_KEY, SIGN_TYPE);

        }
        return alipayClient;
    }

    /**
     * 功能描述: <br>
     * 统一收单线下交易查询
     *
     * @Param: [outTradeNo 优潮订单号 , tradeNo 支付宝订单号]
     * @Return: java.lang.String
     * @Author: zichu
     * @Date: 2019/8/4 10:31
     */
    public static AlipayTradeQueryResponse alipayTradeQuery(String outTradeNo, String tradeNo) {
        AlipayClient alipayClient = getAlipayClient();
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();
        if (StrUtil.isNotEmpty(tradeNo)) {
            model.setTradeNo(tradeNo);
        }
        model.setOutTradeNo(outTradeNo);
        request.setBizModel(model);
        AlipayTradeQueryResponse response;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            return null;
        }
        return response;
    }


    public static void main(String[] args) {
        AlipayTradeRefundResponse alipayTradeRefundResponse = alipayTradeRefund("3000164591912041165786", null, "499");
        System.out.println(JSON.toJSONString(alipayTradeRefundResponse));
    }

    /**
     * 功能描述: <br>
     * 支付宝退款
     *
     * @Author: zichu
     * @Date: 2019/11/11 18:33
     */
    public static AlipayTradeRefundResponse alipayTradeRefund(String orderSn, String aliPaySn, String refundAmount) {
        AlipayClient alipayClient = getAlipayClient();
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        AlipayTradeRefundModel alipayTradeRefundModel = new AlipayTradeRefundModel();
        if (StrUtil.isNotBlank(orderSn)) {
            alipayTradeRefundModel.setOutTradeNo(orderSn);
        }
        if (StrUtil.isNotBlank(aliPaySn)) {
            alipayTradeRefundModel.setTradeNo(aliPaySn);
        }
        alipayTradeRefundModel.setRefundAmount(refundAmount);
        request.setBizModel(alipayTradeRefundModel);
        AlipayTradeRefundResponse response;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            return null;
        }
        return response;
    }


    /**
     * 功能描述: <br>
     * 获得支付宝授权接口地址
     *
     * @param userId
     * @Author: zichu
     * @Date: 2019/9/18 12:04
     */
    public static String getAuthCodeUrl(String userId) {
        String uri = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?app_id="
                + APP_ID + "&scope=auth_user,auth_base&redirect_uri="
                + NOTIFY_TOKEN_URL + "&userId=" + userId;
        return uri;
    }

    /**
     * 功能描述: <br>
     * 获取支付宝access_token及userId。
     *
     * @Param: [code , grantType, refreshToken]
     * @Return: java.lang.String
     * @Author: zichu
     * @Date: 2019/9/18 12:07
     */
    public static AlipaySystemOauthTokenResponse alipaySystemOauthToken(AlipaySystemOauthTokenRequest request) {
        AlipayClient alipayClient = getAlipayClient();
        try {
            AlipaySystemOauthTokenResponse oauthTokenResponse = alipayClient.execute(request);
            return oauthTokenResponse;
        } catch (AlipayApiException e) {
            logger.error("获取支付宝的access_token及userId", e);
        }
        return null;
    }
}
