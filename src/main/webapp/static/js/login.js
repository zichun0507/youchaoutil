layui.define(['layer', 'form', 'tips'], function(exports) {
    var form = layui.form,
        layer = layui.layer,
        $ = layui.$,
        tips = layui.tips;

    //刷新验证码
    var captchaImg = $('.lau-sign-captcha'), captchaSrc = captchaImg.attr('src');
    captchaImg.click(function () {
        $(this).attr('src', captchaSrc + '?_t=' + Math.random());
    });

    //ajax请求出错提示
    $(document).ajaxError(function (event, request, setting) {
        if (request.status === 200) {
            tips.error('Invalid response');
        } else {
            tips.error(request.status + ': ' + request.statusText);
        }
    });

    //登陆
    form.on('submit(login)', function (data) {
        if (!/^\S{5,16}$/.test(data.field.username)) {
            tips.warning('请输入用户名');
            return false;
        } else if (!/^\S{5,80}$/.test(data.field.password)) {
            tips.warning('请输入密码');
            return false;
        } 
       /* 
        else if (!/^\S{4,}$/.test(data.field.captcha)) {
            tips.warning('请输入验证码');
            return false;
        }*/

        //登陆中
        tips.loading('登陆中...', 0, -1);

        //发送登陆表单
        $.post('/api/v1.0/admin/login', data.field, function (resp) {
            if (resp.status == 200) {
            	layer.closeAll('tips');
            	
            	var JsonData = JSON.stringify(resp.data);
            	localStorage.setItem("userinfo", JsonData);
            	console.log(localStorage.getItem("userinfo"));
                tips.success(resp.message, function () {
                    location.href = '/admin/index.html';
                });
            } else {
            	console.log(resp.message);
                tips.error(resp.message, function () {
                    captchaImg.attr('src', captchaSrc + '?_t=' + Math.random());
                });
            }
        }, 'json');

        return false;
    });

    exports('login', {});
});
